from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
import time 

chromeCapabilites = DesiredCapabilities.CHROME.copy()
firefoxCapabilities = DesiredCapabilities.FIREFOX.copy()

driver = webdriver.Remote(
    command_executor='http://localhost:4444/wd/hub',
    desired_capabilities=chromeCapabilites
)

driver.get('https://www.google.com')
element = driver.find_element_by_name("q")
element.send_keys("PFSTech")
element.submit()
print(driver.title)
driver.quit()
