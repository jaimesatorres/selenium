**SELENIUM WITH DOCKER HUB**

DOCKERS TO INSTALL

- `docker pull dosel/zalenium`
- `docker pull elgalu/selenium`

ONCE PULLS ARE DONE, SIMPLY RUN THE FOLLOWING COMMANDS TO RUN AND STOP THE SERVICE

    THEY REQUIRE JQ TO BE INSTALLED, IF YOU'RE GETTING ANY ERRORS SIMPLY USE `brew install jq` or `apt-get install jq`

- `curl -sSL https://raw.githubusercontent.com/dosel/t/i/p | bash -s start`
- `curl -sSL https://raw.githubusercontent.com/dosel/t/i/p | bash -s stop`


TO USE ZALENIUM WITH A CLOUD TESTING PROVIDER

- `export SAUCE_USERNAME=<"your Sauce Labs username">`
- `export SAUCE_ACCESS_KEY=<"your Sauce Labs access key">`
- `curl -sSL https://raw.githubusercontent.com/dosel/t/i/p | bash -s start`

THE FOLLOWING URLS ARE USEFUL

- https://hastebin.com/amudihotif.java #SAMPLE OF MANIN SCRIPT
- http://172.17.0.2:4444/wd/hub #THE URL YOU'LL PROBABLY USE TO CREATE NEW CLIENTS
- http://localhost:4444/grid/admin/live #VCN SERVER OF ZALENIUM
- https://www.seleniumhq.org/download/ #DOWNLOAD PAGE OF SELENIUM'S LIBRARIES
  

THIS IS A PIECE OF CODE TO DEMONSTRATE A BASIC SELENIUM SETUP WITH JAVA

```java
//JAVA LIBRARIES
import java.io.IOException;
import java.net.URL;

//SELENIUM LIBRARIES
import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;

//SELENIUM'S WEB LIBRARIES
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

//INDEPENDENT LIBRARIES DEPENDING ON WHICH BROWSER YOU'RE GOING TO USE
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

//REMOTE WEB DRIVER TO CONNECT YOUR SCRIPT WITH THE DOCKER YOU JUST CREATED
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

//JUST BECAUSE
@SuppressWarnings("unused")

//CLASS' NAME IS PG1
public class PG1 {

    //THROWS IOException TO AVOID AN ERROR WHEN DECLARING THE URLS OF THE RemoteWebDrivers
	  public static void main(String[] args) throws IOException {
        //DesiredCapabilities.firefox(); OR DesiredCapabilities.chrome(); ALSO
        //WORKS, BUT IT'S DEPRECTADED
		Capabilities chromeCapabilities = new ChromeOptions();
        Capabilities firefoxCapabilities = new FirefoxOptions();
        
        //SETUP THE CONNECTIONS TO YOUR HUB, CHANGE THE URL IF NEEDED
	    WebDriver chrome = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), chromeCapabilities);
	    WebDriver firefox = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), firefoxCapabilities);

	    //RUN THE SCRIPT AGAINST CHROME
	    chrome.get("https://www.google.com");
	    WebElement element = chrome.findElement(By.name("q"));
        element.sendKeys("PFSTech");
        element.submit();
        System.out.println(chrome.getTitle());

	    //RUN THE SCRIPT AGAINST FIREFOX
	    firefox.get("https://www.google.com");
	    WebElement elementdos = firefox.findElement(By.name("q"));
	    elementdos.sendKeys("PFSGroup");
	    elementdos.submit();
        System.out.println(firefox.getTitle());
      
        //CLOSE BOTH SESSIONS, THIS IS OPTIONAL!!
	    chrome.quit();
	    firefox.quit();
	  }

}
```

ONCE THE SCRIPT IS EXECUTED, YOU SHOULD BE ABLE TO SEE THE CHANGES IN
`http://localhost:4444/grid/admin/live`



THE ALTERNATIVE TO THIS SCRIPT IS TO EXECUTE PYTHON INSTEAD
https://selenium-python.readthedocs.io/api.html

```python
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
import time 

chromeCapabilites = DesiredCapabilities.CHROME.copy()
firefoxCapabilities = DesiredCapabilities.FIREFOX.copy()

driver = webdriver.Remote(
    command_executor='http://localhost:4444/wd/hub',
    desired_capabilities=chromeCapabilites
)

driver.get('https://www.google.com')
time.sleep(10)
element = driver.find_element_by_name("q")
element.send_keys("PFSTech")
element.submit()
print(driver.title)
driver.quit()
```

Even though is much easier to develop an application in python, it seems like both the connection and the execution times increase drastically.

http://79.147.8.50:7777/yomubuseye.coffeescript