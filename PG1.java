import java.io.IOException;
import java.net.URL;

import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
@SuppressWarnings("unused")
public class PG1 {
	  public static void main(String[] args) throws IOException {
		Capabilities chromeCapabilities = new ChromeOptions();
		Capabilities firefoxCapabilities = new FirefoxOptions();
	    WebDriver chrome = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), chromeCapabilities);
	    WebDriver chrome2 = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), chromeCapabilities);

	    // run against chrome
	    chrome.get("https://www.google.com");
	    WebElement element = chrome.findElement(By.name("q"));
        element.sendKeys("PFSTech");
		element.submit();
		System.out.println(chrome.getTitle());

	    // run against firefox
	    chrome2.get("https://www.google.com");
	    WebElement elementdos = chrome2.findElement(By.name("q"));
	    elementdos.sendKeys("PFSGroup");
	    elementdos.submit();
        System.out.println(chrome2.getTitle());
        
	    chrome.quit();
	    chrome2.quit();
	    
	  }

}
